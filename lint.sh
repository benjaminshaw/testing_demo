#!/bin/bash

threshold=6.0
mkdir score

pylint --disable=E0401 telescope.py | tee score/result.txt
score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' score/result.txt)
echo "Pylint score was $score"
if (( $(echo "$threshold > $score" |bc -l) )); then
    exit 1
fi

