#!/usr/bin/env python

"""
    **************************************************************************
    |                                                                        |
    |                       Avishek's telescope module                       |
    |                                                                        |
    **************************************************************************
    | Description:                                                           |
    |                                                                        |
    **************************************************************************
    | Author: Avishek Basu                                                   |
    | Email : avishek@manchester.ac.uk                                       |
    **************************************************************************
    | Usage: How to use                                                      |
    |                                                                        |
    **************************************************************************
    | License:                                                               |
    |                                                                        |
    | Copyright 2022 University of Manchester                                |
    |                                                                        |
    |Redistribution and use in source and binary forms, with or without      |
    |modification, are permitted provided that the following conditions are  |
    |met:                                                                    |
    |                                                                        |
    |1. Redistributions of source code must retain the above copyright       |
    |notice,                                                                 |
    |this list of conditions and the following disclaimer.                   |
    |                                                                        |
    |2. Redistributions in binary form must reproduce the above copyright    |
    |notice, this list of conditions and the following disclaimer in the     |
    |documentation and/or other materials provided with the distribution.    |
    |                                                                        |
    |3. Neither the name of the copyright holder nor the names of its        |
    |contributors may be used to endorse or promote products derived from    |
    |this                                                                    |
    |software without specific prior written permission.                     |
    |                                                                        |
    |THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     |
    |"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       |
    |LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A |
    |PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT      |
    |HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  |
    |SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        |
    |LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   |
    |DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON       |
    |ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR      |
    |TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  |
    |USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH        |
    |DAMAGE.                                                                 |
    **************************************************************************
"""

import requests
import os
import numpy as np


class pullImage():
    """
    A class that pulls a jpg from the internet to
    demostrate pipelines & unit testing
    """

    _telescopes = {'Lovell': "https://www.jb.man.ac.uk/common/webcam.jpg",
                   'MarkII': "http://www.jb.man.ac.uk/common/mkii.jpg",
                   '7m': "http://www.jb.man.ac.uk/distance/observatory/images/webcam.jpg"}


    def __init__(self, telescope="Lovell"):

        self.telescope = telescope

        # Does our telescope name conform
        # to an allowed list?
        self.validate_scope(telescope)

    @staticmethod
    def validate_scope(scope: str):
        """
        Checks that a valid telescopei
        has been requested

        Parameters
        ----------
        scope : str
            The name of the telescope whose
            image we wish to pull
        """
        if scope not in pullImage._telescopes.keys():
            raise IOError("Invalid telescope")

    def pull(self):
        """
        Pulls image of requested telescope
        """

        # Get url of telescope image
        url = pullImage._telescopes[self.telescope]

        # Set output file name
        output_name = self.telescope + ".jpg"

        # Does a file of this name already exist?
        if os.path.isfile(output_name):
            raise OSError("File {} already exists".format(output_name))

        # If not, request image data from server
        # and write to file
        image_data = requests.get(url).content
        with open(output_name, 'wb') as handler:
            handler.write(image_data)
