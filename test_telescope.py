#!/usr/bin/env python

from telescope import pullImage
import pytest
from pytest import mark

import os

class pullTests:

    def test_default(self):
        """
        Test that pulling the Lovell image
        works as the default
        """
        # Make sure the file doesn't already exist
        filename = "Lovell.jpg"
        if os.path.isfile(filename):
            os.remove(filename)

        # Create instance
        puller = pullImage()
        # Pull image
        puller.pull()
        # Check image was pulled
        assert os.path.isfile("Lovell.jpg")


    def test_lovell_pull(self):
        """
        Test that pulling the Lovell image
        works as requested
        """
        # Make sure the file doesn't already exist
        filename = "Lovell.jpg"
        if os.path.isfile(filename):
            os.remove(filename)

        # Create instance
        puller = pullImage(telescope="Lovell")
        # Pull image
        puller.pull()
        # Check image was pulled
        assert os.path.isfile("Lovell.jpg")

    def test_lovell_file_exists(self):
        """
        Tests that the correct exception is raise
        if the file we're pulling already exists
        """
        # Create a fake file with the name we expect
        open("Lovell.jpg", 'a').close()

        puller = pullImage(telescope="Lovell")

        # We except an OSError to come from
        # the pull() method
        with pytest.raises(Exception):
            puller.pull()

    def test_bad_telescope(self):
        """
        Tests that the correct exception is raised
        if we request an invalid telecope
        """
        with pytest.raises(Exception):
            puller = pullImage(telescope="kslfk")
